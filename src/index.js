const URL_FETCH = "https://source.unsplash.com"


function getRandomNumber() {
    return (Math.floor(Math.random()*100));
  }

async function main(){
    let container = document.querySelector(".container");
    for(let child of container.children){
        let fetchedImage;
        await fetch(`${URL_FETCH}/collection/${getRandomNumber()}`).then((img) => fetchedImage = img);
        if(!fetchedImage){ continue; }
        // let picture = document.createElement("picture");
        // let image = document.createElement("img");
        // image.src = fetchedImage.url
        // image.style.width = "100px";
        // image.style.objectFit = "cover"; 
        // picture.appendChild(image)
        // child.textContent = "";
        // child.appendChild(picture)
        
        let image = document.createElement("img");
        image.src = fetchedImage.url
        image.style.width = "100%";
        image.style.height = "100%";
        image.style.objectFit = "cover";   
        child.textContent = "";
        child.appendChild(image);
    }
}

main();